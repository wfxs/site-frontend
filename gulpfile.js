var gulp           = require('gulp');
var s3             = require('gulp-s3');
var awsCredentials = require('./aws-credentials.json');
var commonHeaders  = {
	'x-amz-acl': 'public-read'
}

gulp.task('upload', ['upload-app', 'upload-static']);

gulp.task('upload-static', function() {
	return gulp.src('./dist/**').pipe(s3(awsCredentials, {
		headers: commonHeaders
	}));
});

gulp.task('upload-app', function() {
	return gulp.src('./static/**').pipe(s3(awsCredentials, {
		uploadPath: "/static/",
		headers: commonHeaders
	}));
});
