import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import config from 'config'

// Vue Modules
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueResource)

// Set config
Vue.config.debug = config.vue.debug
Vue.config.devtools = config.vue.devtools
Vue.http.options.root = config.api.endpoint

// Start Application
require('./bootstrap')
