export function signedIn (state) {
	return !!state.session
}

export function currentUser (state) {
	return state.session
	? state.session.user
	: null
}

export function appFocused (state) {
	return state.app.blockers === 0
}
