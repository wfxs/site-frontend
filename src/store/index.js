import Vuex from 'vuex'
import mutations from './mutations'

export default new Vuex.Store({
	state: {
		session: false,

		app: {
			blockers: 0
		}
	},
	mutations
})
