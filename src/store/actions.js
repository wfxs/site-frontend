import API from 'api'
import * as types from './mutation-types'

export const signin = ({ dispatch }, login, password, user = false) => {
	return API.Auth.authenticate(login, password)
	.then(token => {
		return user
		? Promise.resolve({ data: user })
		: API.Users.get({ id: token.user_id })
	})
	.then(({ data }) => {
		dispatch(types.CREATE_SESSION, data)
	})
}

export const signout = ({ dispatch }) => {
	return API.Auth.remove()
	.then(() => dispatch(types.DELETE_SESSION), (err) => {
		if (err.type === 'not_found') {
			dispatch(types.DELETE_SESSION)
		} else {
			throw err
		}
	})
}

export const rehydrateSession = ({ dispatch }) => {
	dispatch(types.REHYDRATE_SESSION)
}

export const addAppBlocker = ({ dispatch }) => {
	dispatch(types.APP_BLOCKER_ADD)
}

export const removeAppBlocker = ({ dispatch }) => {
	dispatch(types.APP_BLOCKER_REMOVE)
}
