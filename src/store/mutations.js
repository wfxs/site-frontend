import * as types from './mutation-types'
import LocalStorage from 'library/local-storage'

export default {
	[types.CREATE_SESSION] (state, user) {
		state.session = { user: user }
		LocalStorage.setItem('session', state.session)
	},

	[types.DELETE_SESSION] (state) {
		state.session = false
		LocalStorage.setItem('session', null)
	},

	[types.REHYDRATE_SESSION] (state) {
		let session = LocalStorage.getItem('session')
		// state.session = session ? session : false
		state.session = session
	},

	[types.APP_BLOCKER_ADD] (state) {
		state.app.blockers++
	},

	[types.APP_BLOCKER_REMOVE] (state) {
		state.app.blockers--
	}
}
