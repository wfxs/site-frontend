import Vue from 'vue'

export default Vue.extend({
	replace: false,
	data: function () {
		return {
			blocking: true,
			deferredResult: null
		}
	},
	methods: {
		complete: function (result) {
			if (this.deferredResult) {
				this.deferredResult.resolve(result)
			}

			this.$dispatch('modal-close', this)
		},
		close: function () {
			if (this.deferredResult) {
				this.deferredResult.reject()
			}
			this.$dispatch('modal-close', this)
		}
	}
})
