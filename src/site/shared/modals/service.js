import Deferred from 'es6-deferred'

class ModalService {
	registerContainer (container) {
		this.container = container
	}

	open (modal, deferred = true) {
		if (deferred) {
			modal.deferredResult = new Deferred()
		}

		return this.container.open(modal)
	}

	close (modal) {
		this.container.close(modal)
	}
}

export default new ModalService()
