import config from 'config'

class LocalStorage {
	constructor () {
		this._store = window.localStorage
	}

	getItem (key) {
		let item = this._store.getItem(this._formatKey(key))

		if (item == null) {
			return null
		} else {
			return JSON.parse(item)
		}
	}

	setItem (key, value) {
		if (value === null) {
			this.removeItem(key)
		} else {
			this._store.setItem(this._formatKey(key), JSON.stringify(value))
		}
	}

	removeItem (key) {
		this._store.removeItem(this._formatKey(key))
	}

	clear () {
		this._store.clear()
	}

	_formatKey (key) {
		return `${config.app.storageKey}_${key}`
	}
}

export default new LocalStorage()
