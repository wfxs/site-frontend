import Moment from 'moment'

export default function (timestamp) {
	timestamp = Moment(timestamp)
	let monthsDiff = Moment().diff(timestamp, 'months')

	if (monthsDiff > 0) {
		if (timestamp.year() === Moment().year()) {
			return timestamp.format('D, MMM')
		} else {
			return timestamp.format('D, MMM YYYY')
		}
	} else {
		return timestamp.fromNow()
	}
}
