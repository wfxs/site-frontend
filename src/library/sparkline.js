export default function (canvas, data) {
	let ctx = canvas.getContext('2d')
	let height = canvas.height
	let width = canvas.width
	let totalPoints = data.length
	let max = Math.max.apply(Math, data)
	let xstep = width / (totalPoints - 1)
	let ystep = max / height
	let x = 0
	let y = 0
	let i

	ctx.clearRect(0, 0, width, height)

	ctx.beginPath()
	ctx.fillStyle = 'rgba(120,100,170,1)'

	ctx.moveTo(0, height)
	ctx.lineTo(0, height - data[0] / ystep + 1) // 2

	for (i = 1; i < totalPoints; i++) {
		x += xstep
		y = height - data[i] / ystep + 1
		ctx.lineTo(x, y)
	}

	ctx.lineTo(x, height)
	ctx.fill()

	x = 0
	y = 5

	ctx.beginPath()
	ctx.lineWidth = 2
	ctx.strokeStyle = 'rgba(70,40,120,0.05)'
	// ctx.strokeStyle = 'rgba(120,100,170,0.3)'
	ctx.moveTo(0, height - data[0] / ystep)

	for (i = 1; i < totalPoints; i++) {
		x += xstep
		y = height - data[i] / ystep
		ctx.lineTo(x, y)
	}

	ctx.stroke()
}
