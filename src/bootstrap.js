import VueRouter from 'vue-router'
import AppRoutes from './routes'

import store from 'store'
import { rehydrateSession } from 'store/actions'

// Session reload
rehydrateSession(store)

// Routing
new VueRouter({
	hashbang: false,
	history: true
})

// Init Routes
.map(AppRoutes)

// Bootstrap
.start(require('./app'), '#app')
