import Vue from 'vue'

export default Vue.resource('users{/id}')

export function handleExist (handle) {
	return Vue.http.get('users/handle/' + handle)
}
