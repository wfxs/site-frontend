import Vue from 'vue'

export default Vue.resource('articles{/id}')
