import ExtendableError from 'es6-error'

export class AuthError extends ExtendableError {}

export class RequestError extends ExtendableError {
	constructor (error) {
		super(error.message)
		this.type = error.type
		this.errors = error.errors
	}
}

export class ValidationError extends ExtendableError {
	constructor (error) {
		super(error.message)
		this.type = error.type
		this.errors = error.errors
	}
}
