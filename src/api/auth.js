import Vue from 'vue'
import LocalStorage from 'library/local-storage'

class Auth {
	constructor () {
		this.token = LocalStorage.getItem('auth_token')
	}

	setToken (token) {
		this.token = token
		LocalStorage.setItem('auth_token', token)
		Vue.http.headers.common['Authorization'] = this.getAuthHeader()
	}

	getAuthHeader () {
		if (this.token) {
			return `Token ${this.token.hash}`
		} else {
			return null
		}
	}

	authenticate (login, password) {
		return this._requestToken({
			type: 'credentials',
			login: login,
			password: password
		})
	}

	refresh () {
		return this._requestToken({
			type: 'refresh',
			refresh_hash: this.token.refresh
		})
	}

	remove () {
		return Vue.http.delete(`tokens/${this.token.hash}`)
		.then(() => {
			this.setToken(null)
		})
	}

	_requestToken (params) {
		return Vue.http.post('tokens', params)
		.then(({ data }) => {
			this.setToken(data)
			return data
		})
	}
}

export default new Auth()
