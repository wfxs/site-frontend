import Vue from 'vue'

export default {
	Posts: Vue.resource('forum/posts{/id}'),
	Threads: Vue.resource('forum/threads{/id}'),
	Categories: Vue.resource('forum/categories{/id}'),
	Directories: {
		get: Vue.resource('forum/directories{/id}').get
	}
}
