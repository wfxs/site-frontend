import Vue from 'vue'
import * as Errors from './errors'
import Auth from './auth'
import Users from './users'
import Forums from './forums'

Vue.http.interceptors.push({
	response: function (response) {
		if (response.ok) {
			return response
		} else {
			handleError(response)
		}
	}
})

function handleError (response) {
	if (response.data && response.data.error) {
		let error = response.data.error

		switch (error.type) {
		case 'authorisation':
			throw new Errors.AuthError()
		case 'validation':
			throw new Errors.ValidationError(error)
		default:
			throw new Errors.RequestError(error)
		}
	} else {
		throw new Errors.RequestError({ type: 'undefined', message: 'Undefined error' })
	}
}

export default {
	Forums,
	Users,
	Auth
}
