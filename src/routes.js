import MainLayout from 'site/shared/layouts/main-layout'
import SimpleLayout from 'site/shared/layouts/simple-layout'

/* Home Section */
import Home from 'site/home'
/* Forum Section*/
import Forum from 'site/forum'
import ForumThread from 'site/forum/thread'
import ForumDirectory from 'site/forum/directory'
/* Servers Section*/
import Servers from 'site/servers'
/* Support Section*/
import Support from 'site/support'
/* Authentication Section*/
import Signin from 'site/authentication/signin'
import Register from 'site/authentication/register'
import RegisterVerify from 'site/authentication/register/verify'
import RegisterSuccess from 'site/authentication/register/success'

let siteRoutes = {
	'/': {
		name: 'home',
		component: Home
	},

	'/servers': {
		name: 'servers',
		component: Servers
	},

	'/support': {
		name: 'support',
		component: Support
	},

	'/forums': {
		component: Forum,
		subRoutes: {
			'/': {
				name: 'forum',
				component: ForumDirectory
			},
			'/:category': {
				name: 'forum',
				afterUpdate: 'fetchDirectory',
				component: ForumDirectory
			},
			'/:category/:page': {
				name: 'forum-page',
				afterUpdate: 'fetchDirectory',
				component: ForumDirectory
			},
			'/t/:thread': {
				name: 'forum-thread',
				component: ForumThread
			},
			'/t/:thread/:page': {
				name: 'forum-thread-page',
				component: ForumThread
			}
		}
	}
}

export default {
	'/': {
		component: MainLayout,
		subRoutes: siteRoutes
	},

	'/signin': {
		component: SimpleLayout,
		subRoutes: {
			'/': {
				component: Signin
			}
		}
	},
	'/register': {
		component: SimpleLayout,
		subRoutes: {
			'/': {
				component: Register
			},
			'/success': {
				component: RegisterSuccess
			},
			'/verify/:hash': {
				component: RegisterVerify
			}
		}
	}
}
