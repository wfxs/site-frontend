import config from './base'

config.vue.debug = true
config.vue.devtools = true

export default config
