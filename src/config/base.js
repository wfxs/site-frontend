export default {
	app: {
		storageKey: 'wfx'
	},
	api: {
		endpoint: 'https://api.wfx.es'
	},
	vue: {
		debug: false,
		devtools: false
	}
}
