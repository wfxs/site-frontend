import config from './base'

config.vue.debug = true
config.vue.devtools = true

// config.api.endpoint = 'https://locahost:4000' // Local server
// config.api.endpoint = 'http://10.0.0.77:4000' // Testing laptop

console.log('%c Whitefox debug config loaded!', 'color:green;font-weight:bold')

export default config
